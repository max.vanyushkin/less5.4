resource "google_compute_instance" "juneway_vm" {
  count        = var.instance_count
  name         = format(var.machine_name, count.index + 1)
  machine_type = var.machine_type
  zone         = var.zone

  tags = ["vanyushkinm", "juneway", "http-server","https-server"]

  metadata = {
    ssh-keys = "root:${file(var.public_key_path)}"
  }

  provisioner "local-exec" {
    command = "echo ${self.name} ${self.network_interface[0].access_config[0].nat_ip} >> host.list"
  }

  connection {
    type = "ssh"
    user = "root"

    private_key = "${file(var.private_key_path)}"
    host = self.network_interface[0].access_config[0].nat_ip
  }
 

  provisioner "remote-exec" {
     inline = [
       "apt -y install nginx",
       "echo ${self.name} ${self.network_interface[0].access_config[0].nat_ip} > /var/www/html/index.nginx-debian.html"
     ]
  }

  boot_disk {
    initialize_params {
      image = var.image
      size = var.disk_size
    }
  }                                                       

  network_interface {
    network = "default"
    access_config {   
    }
  }
}
 