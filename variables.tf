variable "project" {
    description = "The project name"
    default = "phrasal-hallway-158906"
}

variable "region" {
    description = "The selected region"
    default = "europe-west6"
}

variable "image" {
    description = "The image name/path the machine built from"
    default = "projects/ubuntu-os-cloud/global/images/ubuntu-2104-hirsute-v20210928"
}

variable "disk_size" {
    description = "The size of the disk value to be provisioned"
    default = "20"
}

variable "machine_type" {
    description = "The selected type of machine to be created"
    default = "e2-small"
}

variable "machine_name" {
    description = "The specified machine name"
    default = "node-%s"
}

variable "firewall_name" {
    description = "The specified machine name"
    default = "firewall-%s"
}

variable "zone" {
    description = "The zone for a machine to be provisioned"
    default = "europe-west6-a"
}

variable "instance_count" {
    description = "The machines count to be provisioned"
    default = 3
}

variable "public_key_path" {
    description = "The file path to the private key"
    default = "C:\\Users\\MaxVanyushkin\\.ssh\\primary.pub"
}


variable "private_key_path" {
    description = "The file path to the private key"
    default = "C:\\Users\\MaxVanyushkin\\.ssh\\primary"
}

variable "gce_ssh_user" {
    description = "The GCP VM Username"
    default = "juneway-pro"
}

